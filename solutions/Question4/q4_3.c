/** Public API functions */

#include <stdlib.h>

/*
Function: API_FormatDateTimeString
Description: API Interface function to reformat a US format
 date/time string MM/DD/YYYY into the format Month DD,YYYY
Released: Interface made public in release v1.01 on 01.01.2019.
 Interface changes are not permissible.
Returns: A pointer to the date/time string
*/
const char * API_FormatDateTimeString(const char * inDate)
{
    struct tm date;
    char* formatted_date = (char* )malloc(sizeof(char) * 60);
    strptime( inDate, "%m/%d/%Y", &date );
    strftime( formatted_date, 68, "%B %d, %Y", &date );
    //debug_console_puts( formatted_date );
    return (const char *) formatted_date;
}
