#include <vector>
#include <algorithm>
#include <assert.h>

[[deprecated]] int myFunction(std::vector<int> vector1, std::vector<int> vector2)
{
    int result = 0;
    for (auto x : vector1)
    {
        bool flag = true;
        for (auto y : vector2)
        {
            if (y < x)
            {
                flag = false;
            }
        }
        if (flag)
        {
            result++;
        }
    }   return result;
}

int count_smaller_or_equal(const std::vector<int>& vector1, const std::vector<int>& vector2)
{
    const int min_v2 = *std::min_element(begin(vector2), end(vector2));
    return std::count_if(begin(vector1), end(vector1), [&min_v2](auto element){ return element <= min_v2; });
}


int main()
{
    assert(count_smaller_or_equal(std::vector<int>{1, 2, 3}, std::vector<int>{2, 3, 4}) == 2);
    return 0;
}
