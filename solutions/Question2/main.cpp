#include "matrix.h"
#include <string>
#include <cassert>

int main()
{
    const size_t rows = 3;
    const size_t cols = 3;

    matrix<std::string, rows, cols> m;

    for (size_t row = 0; row < rows; row++)
        for (size_t col = 0; col < cols && col <= row; col++)
            m[row][col] = std::to_string(row) + "," + std::to_string(col);

    return 0;
}
