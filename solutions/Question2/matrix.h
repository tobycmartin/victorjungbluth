#ifndef _H_MATRIX_
#define _H_MATRIX_

#include <array>
#include <exception>

template <typename T, size_t NR, size_t NC>
class matrix
{
private:
    using row_container_t = std::array<T, NC>;
    using container_t = std::array<row_container_t, NR>;

    class row_proxy
    {
    public:
        row_proxy(row_container_t& irow_container) : row_container(irow_container) {}
        T& operator[](size_t col) const
        {
            if (col >= NC || col < 0)
            {
                throw std::out_of_range(
                            "Tried to access invalid column in matrix: "
                            "[0," + std::to_string(NC) + ") -> " + std::to_string(col));
            }

            return row_container[col];
        }

    private:
        row_container_t& row_container;
    };


public:
    row_proxy operator[](size_t row)
    {
        if (row >= NR || row < 0)
        {
            throw std::out_of_range(
                        "Tried to access invalid row in Matrix: "
                        "[0," + std::to_string(NR) + ") -> " + std::to_string(row));
        }
        return row_proxy(container[row]);
    }
    void clear()
    {
        container = {};
    }
private:
    container_t container;
};
#endif // _H_MATRIX_
