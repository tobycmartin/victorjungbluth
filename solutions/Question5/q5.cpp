#include <QString>
#include <QDebug>

// Fixed it, but I still don't think the functionallity matches the function's name
void printLastCharacterUppercase()
{
    //Q_UNUSED(argc)
    //Q_UNUSED(argv)
    QString hello = "Hello";
    QString utf8_char;;
    for (auto localVariable : hello)
    {
        utf8_char = QString(localVariable.toUpper());
        qDebug() << "Character: " << utf8_char;
    }
    Q_ASSERT(hello == "Hello");
    qDebug() << "Last character in string: " << utf8_char;
}


// Better function names

void printEveryCharacterUppercase(int argc, char* argv[])
{
    for(int i = 0; i < argc; ++i)
    {
        QString arg = QString::fromUtf8(argv[0]);
        for (auto c : arg)
            qDebug() << "Character: " << QString(c.toUpper());
    }
}

void printLastCharacterUppercase(int argc, char* argv[])
{
    for(int i = 0; i < argc; ++i)
    {
        QString arg = QString::fromUtf8(argv[0]);
        qDebug() << "Last character in string: " << QString(arg.back()).toUpper();
    }
}
