# Elgato Coding Test
This repository contains my answers to the Elgato Coding Test version 1.0.

## Question 1
As a maintainer of some existing code you have been asked to rewrite the following C++
using standard libraries as far as possible. Your rewritten function should deliver the same
output as the original in all cases.
An example of how this will be tested has been provided in main().

```cpp
#include <vector>
int myFunction(std::vector<int> vector1, std::vector<int> vector2)
{
    int result = 0;
    for (auto x : vector1)
    {
        bool flag = true;
        for (auto y : vector2)
        {
            if (y < x)
            {
                flag = false;
            }
        }
        if (flag)
        {
            result++;
        }
    }   return result;
}
int main()
{
    assert(myFunction(std::vector<int>{1, 2, 3}, std::vector<int>{2, 3, 4}) == 2);
    return 0;
}
```

## Question 2
The following template class is in wide use across a code base for a popular product but
there are indications that there may be problems in some instances. You have been asked
to debug and fix the code. 

```cpp
// matrix.h ; rev. 1
#ifndef _H_MATRIX_
#define _H_MATRIX_
#include <array>
template <typename T, size_t NR, size_t NC>
class matrix
{
private:
    using row_container_t = std::array<T, NC>;
    using container_t = std::array<row_container_t, NR>;
    class row_proxy
    {
    public:
        row_proxy(row_container_t& irow_container) : row_container(irow_container) {}
        T& operator[](size_t col) const { return row_container[col]; }
        
    private:
        row_container_t& row_container;
    };
public:
    row_proxy operator[](size_t row) { return row_proxy(container[row]); }
    void clear()
    {
        container_t dummy;
        container.swap(dummy);
    }
private:
    container_t container;
};
#endif // _H_MATRIX_
```

Typical usage example from the code base:

```cpp
// main.cpp
#include "matrix.h"
#include <string>
int main()
{
    const size_t rows = 3;
    const size_t cols = 3;
    matrix<std::string, rows, cols> m;
    for (size_t row = 0; row < rows; row++)
        for (size_t col = 0; col < cols && col <= row; col++)
            m[row][col] = std::to_string(row) + "," + std::to_string(col);
    return 0;
}
```

## Question 3

The following code snippets contain problems. Describe the output or behavior of each and
suggest how the code could be improved.
### Question 3.1

What is the value of i at the end of this loop? Explain why

```javascript
var i = 5;
for(var i = 0; i < 10; i++) {
    // Do something...
}
console.log(i);
```

### Question 3.2

What will this code do and why?

```javascript
Game.prototype.restart = function () {
    this.timer = setTimeout(function() {
    this.clearBoard();
    }, 1000);
};
```
### Question 3.3

Will the if statement be executed? Explain your reasoning and improve the code.

```javascript
let state;
if(state == null) {
    // Do something...
}
```
## Question 4
### Question 4.1
Review the following code fragment. Provide all detail you can.

```c
#define MIN(a,b) a < b ? a : b
```

### Question 4.2
Review the following code fragment. Provide all detail you can.

```c
char const * const SomeString = "A C coding test during an interview at Elgato";
size_t Length = strlen(SomeString);
char * Dest = (char *) malloc(Length);
strcpy(Dest, SomeString);
```

### Question 4.3
You have been asked to maintain this code which is part of a large project. There's a report
it doesn't work correctly, and you have been requested to debug this and provide a minimal
patch. What is the fault and how would you fix it?


```c
/** Public API functions */
/*
Function: API_FormatDateTimeString
Description: API Interface function to reformat a US format
 date/time string MM/DD/YYYY into the format Month DD,YYYY
Released: Interface made public in release v1.01 on 01.01.2019.
 Interface changes are not permissible.
Returns: A pointer to the date/time string
*/
const char * API_FormatDateTimeString(const char * inDate)
{
    struct tm date;
    char formatted_date[60];
    strptime( inDate, "%m/%d/%Y", &date );
    strftime( formatted_date, 68, "%B %d, %Y", &date );
    debug_console_puts( formatted_date );
    return (const char *) &formatted_date;
}
```

## Question 5
Describe what the following code does and suggest any modifications necessary that you think
might help it do what it should do.

```cpp
#include <QString>
#include <QDebug>
void printLastCharacterUppercase( )
{
    Q_UNUSED(argc)
    Q_UNUSED(argv)
    QString hello = "Hello";
    const char *lastCharacter = nullptr;
    for (int i = 0; i < hello.length(); ++i)
    {
        auto localVariable = hello[i];
        localVariable = localVariable.toUpper();
        qDebug() << "Character: " << localVariable;
        QByteArray characterByte = QString(localVariable).toUtf8();
        lastCharacter = characterByte.constData();
    }
    Q_ASSERT(hello == "Hello");
    qDebug() << "Last character in string: " << lastCharacter;
}

```

## Question 6
Elgato, like many other companies, uses many different micro services to host its web
projects. To ensure high availability, these services are monitored with Prometheus. If a
service experiences high load or is unreachable for a certain period of time, an alert is
triggered.
You have been asked to implement a Status Page that exposes the current system status of
each service. You can use the publicly available API from Prometheus
(https://prometheus.io) to get a list of all services and their current status.
Your project manager has given you 1 hour for this task.
1. Set up a space in your GitHub repository for this project
2. Design the status page:
    * Think about an appropriate UI
    * Consider visitors from most popular devices and browsers
3. Retrieve the current system status via the API
    * Request all targets and their respective health status on page load
    * Update the status data every 30 seconds
4. Consider test scenarios and how to validate the performance of this server

As a Test Server you can use the following Prometheus unit for your task:
http://demo.robustperception.io:9090

© Corsair GmbH
